;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;  Copyright © 2021 - by thchha / Thomas Hage, All Rights Reserved.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(##namespace ("gamrepl#"))
(##include "~~/lib/gambit#.scm")

(define thread-group (make-thread-group "gamrepl-clients"))
;; makes the server inspectable
(define tcp-server #f)

;; client connections
(define sockets '())

(define server-stop
  (lambda ()
    (for-each close-port gamrepl#sockets)
    (for-each thread-terminate! (thread-group->thread-list gamrepl#thread-group))
    (close-port gamrepl#tcp-server)))

(define (server-start #!key
                      (server-name "tcp-repl-server")
                      (port-settings (list
                                       local-port-number: 0
                                       reuse-address: #t))
                      (abandon-timeout 300))
  (define prompt (string-append (substring server-name 0 3) " > "))

  (define (print-string str)
    (let lp ((chars (string->list str)))
      (unless (null? chars)
        (write-char (car chars))
        (lp (cdr chars)))))

  (define (server-loop)
    (with-exception-catcher
      (lambda (ex)
        (write (list 'eval 'raised 'error: ex))
        (newline)
        (force-output)
        #f)
      (lambda ()
        (print-string prompt)
        (force-output)
        (let ((inp (read)))
          (print-string "=> ")
          (if (not (eof-object? inp))
              (let ((res (with-exception-catcher
                           (lambda (ex) (list 'exception: (object->string ex)))
                           (lambda () (eval inp)))))
                (unless (eqv? (void) res)
                  (write res)
                  (newline))
                (force-output)
                (server-loop)))))))

  (set! gamrepl#tcp-server (open-tcp-server port-settings))
  (thread-start!
    (make-thread
      (lambda ()
        (let listen ()
          (let ((socket (read gamrepl#tcp-server)))
            (set! gamrepl#sockets (cons socket gamrepl#sockets))
            (thread-start!
              (make-thread
                (lambda ()
                  (parameterize ((current-input-port socket)
                                 (current-output-port socket))
                    ;; no orphans
                    (input-port-timeout-set! (current-input-port) abandon-timeout)

                    ;; prelude
                    (write '(The interaction channel are not overridden.))
                    (newline)
                    (write '(Thus some precedures need the port to be named explictly if stdout is not assumed.))
                    (newline)
                    (write '(Even though pretty-print works: pp does require naming the port.))
                    (newline)
                    (newline)
                    (write '((pp '(1 2 3) (current-output-port))))
                    (newline)
                    (newline)
                    (write '(Use rlwrap to further improve your experience.))
                    (newline)
                    (write `(connection via tcp/ip to ,server-name established.))
                    (newline)
                    (newline)
                    (force-output)

                    ;; repl
                    (server-loop)

                    (newline)
                    (newline)
                    (write 'abandoned)
                    (force-output))
                  (close-port socket))
                'repl-client
                gamrepl#thread-group)))
          (listen)))
      'server-thread
      gamrepl#thread-group)))
