# gamrepl - tcp-repl for gambit scheme

A tcp-repl for embedding within your applications.
Allows interacting with via a tcp port.

My workflow is the following:
1. run nc in a dedicated tmux pane/window, connected to the server
2. send commands to the pane (via vim-slime)
3. read result
4. repeat at 2.

## TODO

Thanks to sham1 over at #scheme-irc: Just shadow problematic bindings for eval, so that the server has restricted capabilities.

## license

Dual licensed like gambit - choose between LGPL or Apache 2.0.
